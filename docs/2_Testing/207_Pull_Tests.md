# Pull Tests

| Revision | Date | Editor | Contents |
| -- | -- | -- | -- |
| v1 | 2023.12.06 | Kenneth Wraight | Created document |
| v2 | 2023.12.19 | Kenneth Wraight | Update instructions |

Describes the procedures measure pull strengths measurements on various materials at Glasgow.
The procedure takes place in the GLADD1 cleanroom.

## Scope

This is a part in the module production QC process for ITk Strip Modules at Glasgow.


## Purpose

Check complicance and consistency of wirebond across materials.

## Responsibilities

People: TBC

## Equipment

- DAGE 4000Plus Wire bonder in GLADD1


## Procedure

Both wirebond pull tests follow a three step to proceedure:

- __Measurement__: using 4000Plus wire bonding machine in GLADD1
    - this yields an output _csv_ file
- __Conversion__ of 4000Plus format for common data format for analysis and upload using _python notebook_
    - a converted _csv_ file
- __Analysis and upload__
    - the converted _csv_ is uploaded to a webApp where data is analysed and upload to PDB can be performed

### Measure module

This procedure uses the 4000Plus machine in GLADD1.

1. steps of proceedure


### Convert 4000Plus output for analysis

Python notebooks are used to convert the 4000Plus output for analysis. 

??? note "Get scripts repo."

    Access them as follows:

    > git clone https://gitlab.cern.ch/glasgow/strips-procedural-scripts.git

    > cd strips-procedural-scripts/Testing/207_Pull_Tests/

    Then use jupyter or equivalent (e.g. VisualStudio) to run the scripts.

Conversion follows the structure:
    
- select input file path
- run analysis
- output converted file

Follow the instructions in the notebooks. The output should be a formatted _csv_ file ready for analysis.

Two notebooks:

 1. Strips_PullTestConverter
    - Pull Test Conversion:
        - define header and data parts
        - write file to (raw) directory with _processed_ suffix 
    - Upload via [webApp](https://itk-pdb-webapps-strips.web.cern.ch) or _monthlies_ notebook

 2. Strips_PullTestConverter-monthlies
    - Upload converted files to specified components (listed in notebook)
    - Update cluster spreadsheet (link in notebook)


