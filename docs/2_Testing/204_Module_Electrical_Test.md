# ITk Module Electrical Tests

| Revision | Date | Editor | Contents |
| -- | -- | -- | -- |
| v1 | 2022.04.05 | Kenneth Wraight | Created document |

Describes the procedures for Strip Module electrical tests using ITSDAQ at Glasgow.
The procedure takes place in the GLADD2 cleanroom.


# Initialise Module:

## Procedure

1. Switch on Keithley and TTi power supplies, and the Nexys.
   Ensure that Keithley is in RS232 communication mode.
   Ensure the firmware version for testing is FID*.

2.  __In a new terminal__
	- cd /opt/strips/
	- source nexys.sh (make sure the firmware version is up to date)
	- source Single.sh
	- ./RUNITSDAQ.sh (if not in right directory cd $SCTDAQ_ROOT)
	- Check configuration file for the right sensor type is being tested. (Should be e.g. JaneDoe)

3. __On the bench__

	- Insert module into dark box
		- Remove 4 screws at corners (save for later)
		- Take off lid
		- Lift module onto jig
		- Screw in corners (finger tight only)	
			- Use screws around box
			- Avoid carrying items over module without lid
		- Plug in cables - ground the module with screw and add bias with molex cable. Plug in I/O cables (white tabbed cable on the inside).
		- Close box
	- Switch on dry air
	- Make sure chiller is on
	- Cover box (limit noise floor increase form light contamination)
	- Turn lights off in lab
	- Ensure Arduino humidity reading is low (> 10%)

    <figure markdown>
    ![image info](/img/204_Module_Electrical_Test/module_connecctions.jpeg){ width="400" }
    <figcaption>Set up showing the module connections in the dark box.</figcaption>
    </figure>





# Electrical Test:
Uses the AMAC to complete a module I-V, and to also perform a full test to measure the strobe delay, response curve and noise occupancy.

1. __In the GUI__
   - Turn on LV to module using DCS -> Module LV On

2. __In the terminal__
   Run Autoconfig command in terminal ->  Autoconfig(false, false, false). Will need DB credentials.
   - If there is an issue with the power supply configuration, may need to run sudo chmod 666 /dev/tty/*

3. __In the GUI__
   - a. DCS -> AMAC Power On
   - b. DCS -> Module IV Scan (add in humidity and temp info from Arduino reading). For AMAC IV keep DCDC off, and set stage to 'Tested'.
   - c. Test -> Run Full Test (for strobe delay, response curve, and noise occupancy measurements)
   - d. Test -> Run Open Channel Search
   - e. DCS -> HV Ramp Down
   - f. DCS -> AMAC Power Down
   - g. DCS -> Module LV Off

To stop a current scan -> stop, to stop all scans of a set of measurements -> abort.