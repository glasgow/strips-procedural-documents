# Module I-V Scan

| Revision | Date | Editor | Contents |
| -- | -- | -- | -- |
| v1 | 2022.04.05 | Kenneth Wraight | Created document |

Describes the procedures to run IV scans on Strip Modules at Glasgow.
The procedure takes place in the GLADD2 cleanroom.

## Scope

This is a regular part in the testing process of ITk Strip Modules at Glasgow.


## Purpose

Scan the I-V curve of ITk Strip Modules at various stages of assembly as part of QA procedures.

## Responsibilities

People: TBC

## Equipment

- High Voltage power supply: Keithley. Need GPIB setting for HV-tabbed module IV. On Keithley: Menu -> Communication -> GBIB.
Connect red and black cables on the rightmost side of the I/O

When configuring hardware ensure it is GPIB connection (Address KE2410) for supply configuration
Do not need LV/TTi for HV tabbed module as no hybrids!

- PC with LabView VI: LHS (windows) PC.

- Dark box: includes cool jig and dry air for environmental control during testing.

<figure markdown>

![image info](/img/203_Module_IV_Scan/module_IV_setup.jpeg){ width="400" }
<figcaption>Set up showing the power supplies and the dark box.</figcaption>

</figure>



## Procedure

1. Update the inventory from the PDB


2.  __On the GUI__

	a. Input the serial number using barcode scanner
		- Data autofilled
		- Note sensor number (used for results directory structure)
        
	b. Select stage

3. __On the bench__

	- Insert module into dark box
		- Remove 4 screws at corners (save for later)
		- Take off lid
		- Lift module onto jig
		- Screw in corners (finger tight only)	
			- Use screws around box
			- Avoid carrying items over module without lid
		- Plug in cables - ground the module with screw and add bias with molex cable
		- Close box
	- Add dry air - a healthy hiss, but not conversation stopping
	- Make sure chiller is on
	- Cover box (limit noise floor increase form light contamination)
	- Turn lights off in lab

4. __On the GUI__
	- Add temp. and RH info. manually
		- Read from arduino output - RH should be < 10% before starting test
	- Configure measure parameters
		- GLUED ... 700
		- PWB ... 550
		- 2µA compliance (should only need to be set once per session)
	- Run IV
	- Save and quit


__Typical current stages__

- <= 300nA range for noise floor as sensor depletes
- 350V full bias
- <= 200V breakdown maybe build issue
- \> 300V breakdown maybe sensor issue
  

## Arduino Settings
- Bits per second 9600
- Tools -> Serial monitor to see data output