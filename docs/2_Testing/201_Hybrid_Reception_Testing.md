# ITk Hybrid Electrical Tests

| Revision | Date | Editor | Contents |
| -- | -- | -- | -- |
| v1 | 2022.04.05 | Kenneth Wraight | Created document |
| v2 | 2023.11.24 | Kenneth Wraight | Add set-up description |

Describes the procedures to test Strip Hybrids at Glasgow. This procedure takes place in the GLADD2 cleanroom.

## Scope

This is a regular part in the manufacturing process of silicon strip modules at Glasgow.

## Purpose

The wirebonds of module provide the electrical connections inlcuding front-end wires of ASIC (ABC chips) to sensor, data-power wires from hybrid to test frame.

## Responsibilities

People: TBC

## Equipment

- Hybird tester jig
    <figure markdown>
    ![image info](/img/201_Hybrid_Electrical_Test/hybrid_testing_jig.jpg){ width="400" }
    <figcaption>Testing jig without Hybrid Panel</figcaption>
    </figure>

- Power supplies
    - Low voltage: TTi
     <figure markdown>
     ![image info](/img/201_Hybrid_Electrical_Test/LV_PS_off.jpg){ width="400" }
     <figcaption>TTi Low voltage power supply</figcaption>
     </figure>

- Powerboards for testing hybrids
     <figure markdown>
     ![image info](/img/201_Hybrid_Electrical_Test/hybrid_testing_PWBs.jpg){ width="400" }
     <figcaption>Powerboards</figcaption>
     </figure>

## Procedure

Hybrids come on panels of up to six. Powerboards (specifically for testing) are added to the panels for testing. The panel is connected to nexys board firmware and electrical tests are run via ITSDAQ on a linux PC. Ensure the nexys firmware version for testing is FIB*.

Useful links:

- ITSDAQ documentation: [here](https://atlas-strips-itsdaq.web.cern.ch)
- Hybrid testing twiki: [V2](https://twiki.cern.ch/twiki/bin/view/Atlas/ABCStarHybridModuleTestsV2) ([V1](https://twiki.cern.ch/twiki/bin/view/Atlas/ABCStarHybridModuleTests))

### Setting-up

1. Setting up the panel

     a. Place the Hybrid panel on the testing jig
      - secure with corner screw if required
     <figure markdown>
     ![image info](/img/201_Hybrid_Electrical_Test/hybrid_panel_on_jig_noPWB.jpg){ width="400" }
     <figcaption>Hybrid Panel on testing jig</figcaption>
     </figure>

     b. Place a _testing_ powerboard on the hybrid panel in the appropriate position (one powerboard per 2 hybrids) using the powerboard lifting tool:

     <figure markdown>
     ![image info](/img/201_Hybrid_Electrical_Test/PWB_lifting_tool.jpg){ width="400" }
     <figcaption>Powerboard lifting tool</figcaption>
     </figure>

    - Make sure the connection between the pwerboard and the hybrid panel is _flush_
     <figure markdown>
     ![image info](/img/201_Hybrid_Electrical_Test/flush_PWB_connection.jpg){ width="400" }
     <figcaption>Hybrid Panel to Powerboard connection</figcaption>
     </figure>

     - Once the powerboard is mounted you are ready to connect the hybrid panel to the firmware
     <figure markdown>
     ![image info](/img/201_Hybrid_Electrical_Test/hybrid_panel_on_jig_onePWB.jpg){ width="400" }
     <figcaption>Hybrid Panel with Powerboard in position 1</figcaption>
     </figure>

2. Connecting the panel

     a. Use the connector below to attach the Hybrid Pabel to the Nexys board. This supplies the required LV and handles all data connections.

     - connect the panel connector tab to the firmware connection

     <figure markdown>
     ![image info](/img/201_Hybrid_Electrical_Test/hybrid_panel_connector.jpg){ width="400" }
     <figcaption>Hybrid Panel connector tab</figcaption>
     </figure>

     hybrid_panel_connector

     <figure markdown>
     ![image info](/img/201_Hybrid_Electrical_Test/hybrid_PS_connector.jpg){ width="400" }
     <figcaption>Firmware connection</figcaption>
     </figure>

3. Start ITSDAQ

     a. Set up terminal

     - __COMMANDS TO ADD HERE__
     
     b. Turn on LV supply

     - should see I= ~0.22A @ 11V _before_ configuration 

     <figure markdown>
     ![image info](/img/201_Hybrid_Electrical_Test/LV_PS_pre.jpg){ width="400" }
     <figcaption>LV display before configuration</figcaption>
     </figure>

     c. Configure ITSDAQ

     - Should see I= N * ~0.67A @ 11V (N= # of powerboards) _after_ configuration 

     <figure markdown>
     ![image info](/img/201_Hybrid_Electrical_Test/LV_PS_post.jpg){ width="400" }
     <figcaption>LV display before configuration</figcaption>
     </figure>

4. Running Scans: __TBD__


## Documentation

The following information needs to be recorded:

- Date, time (start--end) and operator's name
- Program Number, mass, spacer size if gap adjusted, temperature
- List of parts used:
     - Sensor
     - Hybrid

