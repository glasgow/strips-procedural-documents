# Module Metrology (inc. Bow)

| Revision | Date | Editor | Contents |
| -- | -- | -- | -- |
| v1 | 2023.12.05 | Kenneth Wraight | Created document |

Describes the procedures to run Metrology & Bow measurements on Strip Modules at Glasgow.
The procedure takes place in the GLADD1 cleanroom.

## Scope

This is a part in the module production QC process for ITk Strip Modules at Glasgow.


## Purpose

Scans of ITk Strip Modules to ensure they for the specified package tolerances.

## Responsibilities

People: TBC

## Equipment

- CT100 metrology machine, non-contact profilometer which uses a confocal white light sensor. It has a resolution of 0.035 µm in z and 0.05 µm in x and y.


## Procedure

Both _metrology_ and _bow_ measurments follow a three step to proceedure:

- __Measurement__: using CT100 machine in GLADD1
    - this yields an output _txt_ (_csv_) file for metrology (bow) measurements
- __Conversion__ of CT100 format for common metrology analysis software using _python notebook_
    - a converted _csv_ file
- __Analysis and upload__
    - the converted _csv_ is uploaded to a webApp where data is analysed and upload to PDB can be performed

### Measure module

This procedure uses the CT100 machine in GLADD1.

__Metrology proceedure__

For glue thickness measurement and position of the hybrids and powerboards. Uses CT ASCAN program.

1. Set up module.
   
   - a. Mount module on stage and secure the screws. Push the module up against the screws for allignment.
   - b. Push sensor onto the fine pins on the jig.
   - c. Attach the vacuum cable - these measurements are done under vacuum.

2. Open CT100 software.
   
   - a. To change the focus, go to top tool bar -> sensor sensor settings and maximise the intensity using the +/- sign on the keypad. Make sure you are in sensor focus mode (top left hand ruler icon on keypad). The red dot on the left hand side should be in the centre along the green line. Depending on if starting up from power on, you may need to wait a minute or two for the sensor to warm up. 
   - b. Pick a feature and use the arrows to focus on the feature, or, can start autofocus on the arrow key located on the bottom right hand corner of the square. Make sure you are in camera mode (press the top left hand corner ruler icon again).
   - c. Before taking measurements, do a fiducial scan (top tool bar -> Move to fiducials). Fiducials are the reference markings on the module, and set a window to find them on the module. A corner of the module is taken as a reference at (0,0). Execute matching if the cursor is not centered. Can click on preview and use the mouse wheel to zoom in/out. Use right click -> settings etc, optimise -> to change the lighting for visibility.

3. To measure the heights of components (2D scan) and position of hybrid and powerboards. Go to top tool bar -> Scan all positions.
   
   - a. For height measurements we run a scan along the hybrid edge and on to the surface of the sensor at each ASIC location (between ASICs). Each scan has a localised sensor surface measurement and then average both sides of the ASIC.
   - b. In some instanes, take a 2D raster surface measurement for the height - HX0, HCCX, HX1 & HX2.
   - c. The positions are found using fiducial pattern recognition, made relative to the bottom right-hand corner of the sensor.
   - d. After a scan is complete, can use the red cursor to select the raster required. If the scan has failed, can right click on the scan and add a cursor on the raster to align to what it should be. Then, right click and change back to select + modify to move the red box.

4. To edit fiducials and templates.
   
   - a. Go to teach in -> task -> template to double-check what template the scan should be using. To change the start and end points of a measurement, double click on the measurement name when within teach in. Click on the arrow button to move to the start/end point, then click on the start/end box to set. To change the template, go to teach in -> template -> create, and following this click on autofocus position. 
   - b. If struggling with autofocus, you can change the autofocus position by going to teach in -> task -> fiducials -> XY location for autofocus before scan -> single user defined location. By moving the autofocus position to a lighter area e.g. a bond pad, the software should be able to autofocus more easily.

5. Save as a .txt file and can use barcode scanner to add the module serial number to the file name.


__Bow proceedure__

Use ASCAN to run the measurement and use CT SCAN program to apply masking.

1. Set up module.
   
   - a. Mount module on stage and secure the screws. Push the module up against the screws for allignment.
   - b. Push sensor onto the fine pins on the jig.
   - c. This measurement is not completed under vaccuum.


2. Take bow measurement.
   
   - a. The measurement is 3D scan over the whole module. It is ran on a larger step size between the points, and uses a corse grid over the whole module (takes 200 x 200µm points over the whole surface of the module). Use the Bow Measurement scan file on the ASCAN software to complete the measurement. Once complete, right click on the raster and open with SCAN CT.
   - b. To isolate just the sensor measurement by masking out the hybrid and powerboard, all points > 200 µm are removed, negating any point taken on the powerboard or hybrid (a window is applied to cut everything out between a certain height). On the lower right-hand side of the screen, there is a 'Data Operations' box. Apply a mask to draw a cursor from toolbar around the powerboard and then the hybrid. Following this, click on Replace cursor content -> new value (blank) -> Apply. Also repeat process for the triangular section and use a polygonal cursor for this. 
   - c. To remove all points that are > 200 µm, go again to the Data Operations box -> Cut-Off Filter and set the High Cut Off value to +200µm, and the Low Cut-Off to -200 µm, then click Apply. To ensure that all measurements are in µm, there is a drop down menu at the very top of the screen.
   - d. Check if the data is normalised.
   - e. Can couble click the colour bar for a more detailed colour map (located on the right hand side).
   - f. Save and export using the Tools tab. Export data as csv and make sure to use microns. The barcode scanner can be used to add the module serial number to the filename.
   - g. The points in the scan are reduced for the CDF analysis.

### Convert CT100 output for analysis

Python notebooks are used to convert the CT100 output for analysis. 

??? note "Get scripts repo."

    Access them as follows:

    > git clone https://gitlab.cern.ch/glasgow/strips-procedural-scripts.git

    > cd strips-procedural-scripts/Testing/206_Module_Metrology/

    Then use jupyter or equivalent (e.g. VisualStudio) to run the scripts.

Both Metrology and Bow conversion follow the same structure:
    
- select input file path
- run analysis
- output converted file

Follow the instructions in the notebooks. The output should be a formatted _csv_ file ready for analysis.

### Analyse and upload results

Analysis is done via common code available [here](https://gitlab.cern.ch/gdyckes/metrologyanalysis)

For convenience this is packaged as a webApp [here](https://itk-pdb-webapps-strips.web.cern.ch)
- theme: metrologyApp, page: Metrology Upload

1. login using two access codes
2. navigate to theme: MetrologyApp, page: Metrology Upload
3. upload (converted _csv_) file to webApp
    - select "Bow analysis" if appropriate
4. check output _pdf_ files in the on-page viewer
5. check/edit _json_ in the page if required
    - e.g. add component serialNumber
6. upload test results to PDB


### Performing Recalibration
Use Scan CT program to recalibrate sensor and camera.
Calibration materials are located in the cabinet.
- Go to File -> Camera Offset Calibration
May need to change the focus (set max height to 0 to focus)
This procedure needs to use the sensor, not the camera, when scanning over the calibration dot.

## Powerboard Metrology Procedure (Bow Measurement)
When scanning:
1. Move to centre of sample on flex and move to camera offset, make sure sensor strength is stable
2. Move back to camera mode and click scan
Use e.g. 50um x 50um step size

Analysis:
1. Right click and add line (index mode)
2. Normalize
3. Use Fill data filter - average of surrounding values
4. Use Cut off filter
5. Can add reference and primary cursors

