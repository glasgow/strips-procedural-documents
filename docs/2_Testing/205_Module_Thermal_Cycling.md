# ITk Module Thermal Cycling

| Revision | Date | Editor | Contents |
| -- | -- | -- | -- |
| v1 | 2023.09.26 | Kenneth Wraight | Created document |

Describes the procedures for Strip Module thermal cycling using the Coldbox at Glasgow.
The procedure takes place in the GLADD2 cleanroom.

## Scope

Part of module QC.

## Purpose

Thermally cycle modules to check stability.

The coolbox control is linked to ITSDAQ so that electrical scans can be (iteratively) run at different temperatures to measure opreation conistency.

## Documentation

Official Cold box documentation is found [here](https://gitlab.cern.ch/ColdJigDCS).

Includes information on hardware and software set-up and GUI configuration.

## Equipment

<!-- <figure>
    <img src="/Users/kwraight/Glasgow_repositories/strips-procedural-documents/img/Coldbox.jpg"
         alt="thing">
    <figcaption> Probe Station Setup </figcaption>
</figure> -->


- Coldbox set-up: 
    - chiller: mono-phase cooling solution
    - raspberryPi: controlling connections and communications
    - big shiney box: where the magic happens
<!-- Shown in Figure \ref{Figure: Probe Station}. -->

- Nexys board: ITSDAQ firmware

- Cambridge HV interlock box: keeps you safe!

- Controlling PC
    - linux OS for controlling (Nexys) firmware, running ITSDAQ with coldbox 

## Procedure

### fits-daq: fake its-daq

To run the fake its-daq server on rPi:

1. Open new rPi terminal and source environment:

    > cd ~/work/coldbox_controller_webgui/

    > pipenv shell

    > source ~/work/coldjiglib2/configs/UK_China_Barrel/setenv.sh 

    > source ~/work/coldbox_controller_webgui/setenv.sh

2. Run the faker script:

    > cd ~/work/coldjiglib2

    > python Test/run_test_itsdaq_server.py configs/UK_China_Barrel/warwick_influx2.ini  

    - the positional argument specifies the file with influx information: *warwick_influx2.ini* should be correct

3. Run GUI in 2nd terminal

    __Old Software__
    > cd ~/work/coldbox_controller_webgui/

    > pipenv shell

    > source ~/work/coldjiglib2/configs/UK_China_Barrel/setenv.sh 

    > source ~/work/coldbox_controller_webgui/setenv.sh

    > source run_glasgow.sh

    __New Software__
    > cd ~/work/coldjig-6.2.0/coldjig_workspace/UK_China_Barrel/

    > pipenv run coldbox_controller_webgui.py -c configs/glasgow_config.ini

### Every day senario

(Assuming things running as usual)

1. On control PC:

    a. Boot ITSDAQ firmware

    - open a terminal (e.g. Konsole) and go to the local strips directory:
    > cd /opt/strips/

    - boot the Nexys board:
    > source nexys.sh

    - run thermal cycling script (should change to _itsdaq-sw_ directory):
    > source TC.sh

    - run influx DAQ script:
    > ./INFLUX_DAQ.sh

    <!-- add picture -->

	b. Boot AMAC

    - open new terminal or tab and go to the local strips directory:
    > cd /opt/strips/

    - run thermal cycling script (should change to _itsdaq-sw_ directory):
    > source TC.sh

    - run influx DAQ script:
    > ./INFLUX_AMAC.sh

    <!-- add picture -->

2. On raspberryPi

    -  Start GUI

        - login to raspberryPi:
            - __NB__ password required
        > ssh pi@194.36.1.65
        - if you see error: _Too many authentication failures_, then login through school's network

        ??? note "Log in through school's network"
            
            > ssh USERNAME@ppelogin2.ppe.gla.ac.uk

            - then to raspberryPi
            
            > ssh pi@194.36.1.65

        __Old Software__
        - go to GUI directory
        > cd work/coldbox_controller_webgui

        - start python shell
        > pipenv shell

        - run GUI script
        > ./run_glasgow.sh

        __New Software__
        - go to directory
        > cd work/work/coldjig-6.2.0/coldjig_workspace/UK_China_Barrel/

        - run script
        > pipenv run coldbox_controller_webgui.py -c configs/glasgow_config.ini
        

    - open browser to [194.36.1.65:5000](194.36.1.65:5000)
     
    <figure markdown>
    ![image info](/img/205_Module_Thermal_Cycling/control_panel.png){ width="400" }
    <figcaption>Control panel page of TC GUI</figcaption>
    </figure>

    <figure markdown>
    ![image info](/img/205_Module_Thermal_Cycling/advanced_panel.png){ width="400" }
    <figcaption>Control panel page of TC GUI</figcaption>
    </figure>


3. In a browser:

    1. Check Thermal Cycle settings in _Advanced_ tab:

        - High temp: 20ºC

        <figure markdown>
        ![image info](/img/205_Module_Thermal_Cycling/TC_settings.png){ width="400" }
        <figcaption>Thermal Cycle settings</figcaption>
        </figure>

    2. In _Control Panel_ tab:

        - Set _# of cycles_
        - Select chucks
            - at this point is it possible to click _Start_ button
        - Click _Start_ button
            - wait slightly longer than you expect to
            - should see _Log_ box react eventually
            - st this point it is possible to click _Start TC_ button
        - Click _Start TC_ button

        <!-- add picture -->

        - __NB__ to exist cycle gracefully press _Stop TC_

    __NB__ In the event of stopping the Thermal Cycle _unexpectedly_ (i.e. before the end of loops) peltiers have to be returned to a safe state _manually_

    - use the _Advanced_ tab of the GUI
        - set Peltiers voltage to 0V

        <figure markdown>
        ![image info](/img/205_Module_Thermal_Cycling/peltier_settings.png){ width="400" }
        <figcaption>Safe Peltier settings</figcaption>
        </figure>

## (ITSDAQ) Results

The results are accessible (from PPEPC2):

- from commandline:
> cd /nfs/atlas/itk-atlas/strips/data/Electrical/TCModule/SCTVAR/

- from (Home) Desktop:
    - click _Link to strips_ icon
        - opens file borwser
    - data
    - Electrical
    - TCModule
    - SCTVAR

### ITSDAQ protocol

Each time ITSDAQ is started, the run number is incremented by 1, and recorded in:

$SCTDAQ_VAR/results/etc/LAST_RUN.txt 

Each scan is then given a number in the run, where a scan is 1 action of ITSDAQ. With every test run, files are then named:

- $SCTDAQ_VAR/data

    - stlogXXX.txt
    - strunXXX_Y.root

- $SCTDAQ_VAR/results

    - DETfilename_XXX_Y.txt
    - DETfilename_YYYYMMDD.txt

- $SCTDAQ_VAR/ps

    - DETfilename_ScanName_YYYYMMDD_HHMMSS.pdf

More information on ITSDAQ is available [here](https://atlas-strips-itsdaq.web.cern.ch/testing/Outputs.html)

## Finally

When done with the thermal cycling set up:

1. Turn off chiller
    - head
    - tank
2. Turn of Nexys board

## To manually ramp bias (MODULE DEBUGGING)
HVSupplies[0]->Ramp(-50, 2)
(bias, compliance)
