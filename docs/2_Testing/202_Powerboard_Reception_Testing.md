# Powerboard electric test

| Revision | Date | Editor | Contents |
| -- | -- | -- | -- |
| v0 | 2022.04.05 | Kenneth Wraight | Created document |
| v1 | 2023.11.06 | Kenneth Wraight | First draft |
| v1.1 | 2023.11.07 | Kenneth Wraight | Add images and how to update firmware |

This describes the procedures to run reception tests on Powerboard Carrier Cards at Glasgow. This procedure takes place in the GLADD1 cleanroom.

<figure markdown>
   ![image info](/img/202_Powerboard_Electrical_Test/tester_with_carrier.jpg){ width="400" }
  <figcaption>Tester with Powerboard Carrier Panel</figcaption>
</figure>

## Scope

This is a regular part in the manufacturing process of silicon strip modules at Glasgow.


## Purpose

Powerboards are used in module assembly at Glasgow. Powerboards are shipped in panels of ten powerboards. These are tested upon reception to ensure no damage in transit and identify which components are suitable for assembly.


## Responsibilities

People: TBC


## Equipment and tools

- Powerboard tester
    <figure markdown>
    ![image info](/img/202_Powerboard_Electrical_Test/tester.jpg){ width="400" }
    <figcaption>Tester without Carrier Panel</figcaption>
    </figure>

- Power supplies
    - Low voltage: TTi
        <figure markdown>
    ![image info](/img/202_Powerboard_Electrical_Test/LV.jpg){ width="400" }
    <figcaption>TTi Low voltage power supply</figcaption>
    </figure>

    - High voltage: Keithley
    <figure markdown>
    ![image info](/img/202_Powerboard_Electrical_Test/HV.jpg){ width="400" }
    <figcaption>Keithley high voltage power supply</figcaption>
    </figure>




## External Documentation

Slides [here](https://docs.google.com/presentation/d/1-WVlZ4b4PptSjBrUzVuSk3zCSu9teVL90o5rD57MDbU/edit#slide=id.g9311e0b5c3_0_19)


## Procedure

This section is a summary of the [external documentation](https://docs.google.com/presentation/d/1-WVlZ4b4PptSjBrUzVuSk3zCSu9teVL90o5rD57MDbU/edit#slide=id.g9311e0b5c3_0_19) for Powerboard Carrier testing and updating the testing firmware. Please refer to this as required.

### Testing

Basic Procedure (TODO: add pictures):

- Access control GUI:

    - Turn on Zturn using switch on custom PCB

    <figure markdown>
    ![image info](/img/202_Powerboard_Electrical_Test/Zturn_PS.jpg){ width="400" }
    <figcaption>Zturn power supply</figcaption>
    </figure>

    - Use browser to access [http://194.36.1.111:5000](http://194.36.1.111:5000)

    ??? note "Access PCB Testing GUI "
    
        - usr: admin
        - pwd: test


- Initial checks:

    - Turn on LV (TTi) and HV (Keithley) 

    - Run system initialisation test _before_ board attached
        - Click _SYSTEM INITIALIZATION_ (sic, Amerikanski spelling!)
        - Should see: PS initialization OK. I2C check OK. System is ready for tests.
            - If not check the terminal output at the bottom of the web page

- Attach board:
    - Undo (four) screws from Carrier tray
    - Place Powerboard Carrier Card in tray
        - make sure Kapton sheet under Carrier Card
        - align Carrier Card holes with Kapton sheet holes with tray holes
        - make sure Carrier Card is flush with Kapton sheet and tray
    - Push Carrier Card into testing card
        - make Carrier card is flush with tray so connection is aligned
        - push trays together (with a little strength)

    <figure markdown>
    ![image info](/img/202_Powerboard_Electrical_Test/connection.jpg){ width="400" }
    <figcaption>Connected boards of tester and powerboard carrier</figcaption>
    </figure>


- Set-up test in GUI:

    - Use _ITkPDB_ tab to login in to PDB 
        - use two access tokens
    - In _Testing_ tab get carrier info
        - input Powerboard Carrier Card ID into _testPanel_ field and click _AUTO FILL_
            - Carrier and Powerboard information will be retrieved from PDB
        - check version number and PWB number agree with retrieved info.
    
- Run carrier tests:
    - Click _RUN BASIC FUNCTIONALITY TEST_
        - top of the page should go green and announce tests running
    - Scroll down to check spreadsheet cells
        - each cell will go green for pass, yellow for problem, red for fail
    - For 10 Powerboards the test procedure should ~20 minutes
    - At the end:
        - the spreadsheet should be populated with coloured cells
        - the green bar at the top of the page should disappear
        - the terminal will stay _POWER OFF_

- Upload to Production Database

    - Click _UPLOAD TEST RESULTS_
        - When completed, see message: Upload finished for x PBs (y files), where:
            - x is the number of powerboards you selected in the GUI
            - y is 2*x if you are only doing “Basic Functionality Test”

### Update testing firmware

Basic Procedure (TODO: add pictures):

- Download firmware files: 
    - download [link](https://labremote.web.cern.ch/labRemote/powerboard/firmware/)
    - select **zturn_7020** files for preferred version: _BOOT.bin_, _image.ub_, _boot.scr_

- Upload to Zturn via GUI
    - access GUI: [194.36.1.111:5000](http://194.36.1.111:5000)
    - navigate to _Configuration_ tab on top bar
    - drop files (one at a time) into the upload box

- Reboot Zturn
    - Hit reboot button
        - should see message about rebooting under button after a few moments
    - Can reconnect with browser refresh ~2minutes after reboot command

- Can manually check configuration version
    - > ssh petalinux@194.36.1.111
        - pwd: favourite project

    - Check files on Zturn
	    - > less /etc/pwb-release
            - something like: 1.5.0
        - > less /etc/version
            - something like: 20220122023927 (%Y%m%d%H%M%S)


## Marked Powerboards


During production, powerboard will be marked with coloured stickers to indicate failed and troublesome PBs

 - If a powerboard failed tests during QC, it will be marked with 2 red stickers: 
   - a big one over the HV caps, one over the tape on the shieldbox
 - Do not use them to build module
 - Do not test them (testing will be automatically disabled if you use the Auto Fill button in the GUI: requires firmware >= v0.2.30)
   - They will be put into “FAILED” stage in itkDB



  - If a powerboard failed tests but are deemed by an expert as B-grade, it will be marked with a yellow sticker (over the tape on the shieldbox)
    - Do not use them to build module unless told so by ITk Strips management 
    - You need to test them, but you might see some failed tests for those powerboards
    - They will have a “BGrade” flag in itkDB

## Document

Everything needs to be documented in the database per the instructions above. Add any action performed as a comment to the affected module. Add supporting imagery if needed.

