# Module assembly: Wirebonding of Modules

| Revision | Date | Editor | Contents |
| -- | -- | -- | -- |
| v1 | 2022.04.05 | Kenneth Wraight | Created document |

Describes the procedures to wirebonding of modules using the Hesse BondJet820. The procedure takes place in the GLADD clean room at Glasgow.

## Scope

This is a regular part in the manufacturing process of silicon strip modules at Glasgow.


## Purpose

The wirebonds of module provide the electrical connections including front-end wires of ASIC (ABC chips) to sensor, data-power wires from hybrid to test frame.

## Responsibilities


## Equipment

- Wirebonder: Hesse BondJet820
- Bond head:
- Bond wedge:
- Chuck:
- Bond wire: aluminium 25 µm diameter, C.C.C. spool
- tweezers for wire handling

## Procedure

### Select the appropriate program at the BondJet820

- Turn on the bonder(SOP-102).
- Select a program and set the parameter of bonder as:

| Parameter | Unit | ASIC | sensor | PCB | TestFrame |
| -- | -- | -- | -- | -- | -- |
| Deformation | % | 33 | 30 | 25 | 25 |
| BondForce | cN | 27 | 20 | 30 | 30 |
| UltraSonic | % | 24 | 20 | 140 | 110 |

| No. | Program | Loop Height($\mu$m) |
| -- | -- | -- |
| Row1 | atlas2$\times$2\_FE1\_Long.bpx | 150 |
| Row2 | atlas2$\times$2\_FE2\_Long.bpx | 370 |
| Row3 | atlas2$\times$2\_FE3\_Long.bpx | 590 |
| Row4 | atlas2$\times$2\_FE4\_Long.bpx | 800 |
 
- Height check to ensure safety.
-  Fix:
- Setting the focus:
- Height check:
- Wire feed:
- Bonding:
- Check the result:

### Exception handling

The bonding procedure may not run through smoothly. The following list instructs how to deal with exceptions:

- __Missed wire__: Bonder should normally detect this and halt the program. Inspect the area. Remove any wire debris with tweezers. Retry bonding. If still unsuccessful, skip wire and proceed. Handle issue later.
- __Handling of skipped wires__: If immediate re-bonding was unsuccessful, retry bonding in manual mode. And make an explicit remark in the database.


## Documentation

The following information needs to be recorded and upload them to database:

- Date, time (start--end) and operator's name.

