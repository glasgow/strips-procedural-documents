# Barrel Module Assembly

| Revision | Date       | Editor          | Contents |
|----------|------------|-----------------| -- |
| v1       | 2022.04.05 | Kenneth Wraight | Created document |
| v2       | 2023.11.10 | Kenneth Wraight | Fix links formats |
| v3       | 2024.01.28 | Andy  Blue      | Fix links formats |

<!-- 
Latex style...
- inline:
When \(a \ne 0\), there are two solutions to \(ax^2 + bx + c = 0\) and they are
- centred newline:
$$x = {-b \pm \sqrt{b^2-4ac} \over 2a}.$$ 
-->


This document will briefly describe the standard operating procedures of how to assemble the modules at Glasgow.

## Module Assembly

The construction of a complete module mainly consists of 7 steps, as described in the following.

### 1. Hybrid Preparation

Take the Hybrid transport box from the N<sup>2</sup> Cabinet. 

Remove the Hybrid panel from transport box. Four fly screws hold the panel upside down. 

<figure markdown>
   ![image info](/img/101_Module_Assembly/HYBRID_TEST_PANEL_transport_box.jpg){ width="400" }
  <figcaption>Hybrid panel transport box</figcaption>
</figure>

<figure markdown>
   ![image info](/img/101_Module_Assembly/HYBRID_TEST_PANEL_in_transport_box.jpg){ width="400" }
  <figcaption>Hybrid panel in transport box</figcaption>
</figure>

Attach the Hybrid panel to the assembly Jig. 

Select the Hybrid to remove, and note the position. 

Apply the vacuum for the Hybrid position.  

Begin to prepare the hybrid as follows:

- Remove Kapton tape on sides of Hybrid
- Remove Kapton tape on top and bottom tabs of Hybrids
- Remove wire bonds between the hybrid and panel 

Using tweezers pick up the hybrid by a tab and position it on the balance
Measure the mass of the Hybrid

Lift and place the Hybrid  on to the Hybrid Positioning jig and apply vacuum. 

Cut both tabs with the tab tool. 

<figure markdown>
   ![image info](/img/101_Module_Assembly/HYBRID_with_wirebonds_removed.jpg){ width="400" }
  <figcaption>Hybrid with wirebonds removed</figcaption>
</figure>

<figure markdown>
   ![image info](/img/101_Module_Assembly/HYBRID_on_positioning_jig.jpg){ width="400" }
  <figcaption>Hybrid on positioning jig</figcaption>
</figure>

<figure markdown>
   ![image info](/img/101_Module_Assembly/tab_cutting_line.png){ width="400" }
  <figcaption>Tab cutting line</figcaption>
</figure>

place the tabs on to the balance and meaure their mass

Place the Hybrid pick up tool on top, and remove vacuum from the positioning jig 

Apply vacuum to the tool, and pick up the hybrid. 

Place the pick up tool on the tool rest. 

Add the stencil on top of the hybrid (see below). 
- The stencil has a particular orientation with respect to the hybrid. The single long glue stripe must be oriented along the side where the front-end ASIC bonds will go. To help get the orientation right, there is a 'T' marker on the stencil and on the pickup tool, as well as differently sized dowel pin holes to prevent wrong orientations. 

<figure markdown>
   ![image info](/img/101_Module_Assembly/HYBRID_with_stencil.jpg){ width="400" }
  <figcaption>Hybrid with Stencil</figcaption>
</figure>

Return the hybrid panel to transport box and place back in the N<sub>2</sub> cabinet.


### 2. Prepare the Sensor

Remove the module transport box from the N<sup>2</sup> cabinet.

Remove the top four washers, and remove the top perspex lid. 

Lift the sensor - attached to the white frame and green test frame - and palace on the wire bonding jig. 

Apply vacuum to the bonding jig.

Remove the four screws that attach the white frame to the test frame. 

With vacuum on to the jig, remove the white frame from the sensor (there are four suction cups) (see below).

<figure markdown>
   ![image info](/img/101_Module_Assembly/SENSOR_with_frame_removed.jpg){ width="400" }
  <figcaption>Sensor with white frame removed</figcaption>
</figure>

<figure markdown>
   ![image info](/img/101_Module_Assembly/SENSOR_on_module_assembly_jig.jpg){ width="400" }
  <figcaption>Sensor on Module Assembly Jig</figcaption>
</figure>

Now remove the two bonds (for the HV and Ground) on the sensor (top right and bottom right). 

Remove the vacuum to the wire bonding jig. 

Pick up the sensor with the module pick up tool. 

Place the sensor on the black odule plate and measure the mass in the balance

remove the sensor from the plate and onto the module assembly jig, making sure the corner is tight against the three pins. 
- For reference, the sensor should be placed so that HV Tab sits in the cutout of the jig (Fig \ref{fig:Sensor2}). 

Apply vacuum to the jig, and remove vacuum to the module pick up tool, and remove the pick up tool.


### 3. Mix the Glue

Mix glue for 5 minutes, remove from bi pack and leave for 75 mins

### 4. Assemble the Hybrid

Apply the glue with the ruler. Aim for:

 - first pass to distribute the glue
 - second pass to remove the residual glue

Remove the Stencil. 

Visually inspect the glue pattern (checking by eye for bubbles or spots with excessive/missing glue). 

Use a cocktail stick to burst any bubbles. 

Take the pickup tool (with the hybrid attached) and turn it upside down and place on the Sensor. 
- The dowel pins on the pick up tool pass through the alignment holes on the jig. Use T markers on the tool and on the jig to get the orientation right. Make sure to check that the pickup tool is fully down, with the pick-up tool’s landing pads touching the jig. 

Place the weight on top of the jig, making sure the vacuum lead to the pick up tool sits on the hybrid rest tool, so as not to pull on the connection.

<figure markdown>
   ![image info](/img/101_Module_Assembly/HYBRID_with_glue.jpg){ width="400" }
  <figcaption> Hybrid with Glue</figcaption>
</figure>

<figure markdown>
   ![image info](/img/101_Module_Assembly/HYBRID_on_MODULE_with_weight.jpg){ width="400" }
  <figcaption> Hybrid on Module with weight</figcaption>
</figure>

The barrel module construction uses two sets of holes: one for short-strip modules and a second set for long-strip. The module assembly jig is engraved to label which set of holes must be used for which tool. (LS-X for the LS Module, and SS-X and SS-Y for the SS modules) 


### 6. Prepare the Powerboard

As with the Hybrids, remove the Wire bonds and kapton tape from the desired powerboard on the powerboard panel. 

Weigh the powerboard, and place on the flow cabinet bench top. 

Pick up the powerboard, and place on the powerboard alignment tool. 

Then use two cocktail sticks to align the 2 holes in the powerboard to the two holes in the alignment jig, and apply vacuum to the jig. (\ref{fig:PB1})


<figure markdown>
   ![image info](/img/101_Module_Assembly/PWB_alignment.jpg){ width="400" }
  <figcaption> Powerboard being aligned on the jig</figcaption>
</figure>

<figure markdown>
   ![image info](/img/101_Module_Assembly/PWB_stencil_on_tool.jpg){ width="400" }
  <figcaption> Powerboard stencil on tool</figcaption>
</figure>

Attach the powerboard pick up tool to the powerboard, un-vacuum the alignment jig and apply vacuum to the pick tool. 

Pick up the powerboard tool and rest it on the the rest tool. 

Add the powerboard stencil on top of the paperboard. (\ref{fig:PB2})


### 7. Assemble the Powerboard

Apply the glue with the ruler. Aim for:

- 1 pass to distribute the glue
- 2nd pass to remove the residual glue

Remove the Stencil. 

Visually inspect the glue pattern (checking by eye for bubbles or spots with excessive/missing glue). 

Use a cocktail stick to burst any bubbles. Take the pickup tool (with the powerboard attached) and turn it upside down and place on the Sensor. 

The dowel pins on the pick up tool pass through the alignment holes on the jig. 

Use T markers on the tool and on the jig to get the orientation right. 

Make sure to check that the pickup tool is fully down, with the pick-up tool’s landing pads touching the jig. 

Place the weight on top of the jig, making sure the vac lead to the pick up tool sits on the hybrid rest tool, so as not to pull on the connection.


## Documentation

The following information needs to be recorded:

- Date, time (start--end) and operator's name
- Program Number, mass, spacer size if gap adjusted, temperature
- List of parts used:
     - Sensor
     - Hybrid
