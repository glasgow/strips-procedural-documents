# Master SOP

__Glasgow__ ITk Standard Operation Procedure

### Revision history
| Revision | Date | Editor | Contents |
| --- | --- | --- | --- |
| v1 | 2022.04.05 | Kenneth Wraight  |  Created document |
| v2 | 2023.09.20 | Kenneth Wraight | Migrate to mk-docs |
| v3 | 2023.12.06 + | Kenneth Wraight | Maintaining |

This master document describes the operations at Glasgow for manufacturing and testing strip modules within the _ATLAS ITk Phase-II_ upgrade project. It describes responsibilities, local organization and the documentation structure.

## Scope

The ATLAS group at Glasgow hosts one of the manufacturing sites for ATLAS ITk strip modules to be used in the upgraded ATLAS Inner tracker detector. Most of the activities will be carried out in the GLADD1 and GLADD2 labs. 

## Purpose

This document and the herein listed subdocuments describe the procedures in place to manufacture ITk Strip modules to high standards and principles to assure their quality.


## Definitions 

The definitions defined here apply to all sub-documents as well.

| Abbr. | Expansion | meaning |
| -- | -- | -- |
| ITk | Inner Tracker | Consists of pixel and strip trackers |


## Responsibilities

Includes:

 - the __main responsible__ is the _team leader_
    - Currently: Andy Blue 
 - the __responsible for operations__ maintains the SOP documents current, organizes the proper training and makes sure all members of the team follow the SOP. Reports to the _main responsible_.  
    - Currently:  ??
 - __team members__ are responsible to follow the instructions described in the SOP documents for which they are trained. They are not allowed to carry out any procedure without proper and documented training. Pre-production is considered training phase where procedures are allowed to be carried out without documented training. Team members report to the _responsible for operations_
    - The list of current _team members_ is maintained by the _responsible for operations_


## Document hierarchy

The documents are organised in a hierarchical way:

<figure markdown>
   ![image info](/img/000_Overview/SOPhierarchy.svg){ width="500" }
  <figcaption>SOP documentation map</figcaption>
</figure>

Documents are identified by a 3-digit number. The top-level document has the number 000. All the other documents carry numbers different from this. The first digit identifies the group the document belongs to

 
### Referencing documents

- Cross-reference within SOP documents go as SOP~XXX, where XXX is the document number. This assumes a reference to the latest version of that document. References to specific version are to be avoided.
- Referencing from external documents should use the full title and SOP number.

### Scripts

Some procedures involve scripts for data-taking/analysis. These can be found in a [sister repository](https://gitlab.cern.ch/glasgow/strips-procedural-scripts) with further instructions in the appropriate procedural document.

### List of documents

Table shows the existing documents. A list of all current documents is to be maintained and made accessible to all people involved.

