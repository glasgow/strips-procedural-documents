# Useful Links

__Glasgow__ ITk Standard Operation Procedure

### Revision history
| Revision | Date | Editor | Contents |
| --- | --- | --- | --- |
| v1 | 2024.01.19 | Kenneth Wraight  |  Created document |

A list of links with some order.

## Official Documentation

__"Made by experts"__

### General

[ITk-docs](https://itk.docs.cern.ch)

### Hardware

[ITSDAQ](https://atlas-strips-itsdaq.web.cern.ch/index.html)

### Software


## Unofficial Documentation

__"Made by users"__

### Hardware

### Software


## Local links

[Googlesheet inventory](https://docs.google.com/spreadsheets/d/1nAngJBklxRyd0oDuUzrVVPphMAtbAGv2rlmIoQK1a8E/edit#gid=1684296784)


## Miscellaneous



