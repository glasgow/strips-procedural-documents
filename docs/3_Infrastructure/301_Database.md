# Production Database

| Revision | Date | Editor | Contents |
| -- | -- | -- | -- |
| v1 | 2022.04.05 | Kenneth Wraight | Created document |
| v2 | 2024.02.28 | Kenneth Wraight | Major revision |

Describes the procedures to access the ITk Production Database (PDB).

## URL address and sign in

Go to unicorn web application: [here](https://itkpd-test.unicorncollege.cz/)

<figure markdown>
![image info](/img/301_Database/unicorn-front.png){ width="400" }
<figcaption>Front page of unicorn web application</figcaption>
</figure>

Login methods:

- using two database passwords
- Cern single sign-on (SSO)
- google account (if set-up)

??? "Setting PDB passwords"

    If you require PDB passwords to access:
    
    1. Go to [unicorn website](https://itkpd-test.unicorncollege.cz/)
    2. Select the Plus4U button (top righthand side)
    3. Select “Cannot log in?” in the pop-up (bottom righthand side)
    4. Input your email to receive instructions to finish registration
    
    For screenshots please follow the instructions [here](https://indico.cern.ch/event/1350108/sessions/520750/attachments/2765634/4817284/PDB_access.pdf)

<figure markdown>
![image info](/img/301_Database/unicorn-sign-in.png){ width="400" }
<figcaption>Select sign-in method</figcaption>
</figure>

Should see institution dashboard (for Glasgow) when signed-in

<figure markdown>
![image info](/img/301_Database/unicorn-dashboard.png){ width="400" }
<figcaption>Institution dashboard page</figcaption>
</figure>

## Project Specific Tools

You can find a list of PDB tools in [itk-docs](https://itk.docs.cern.ch/general/PDB_tools/General_PDB_tools/)

 - includes list of Strips tools
 
## To Make a Shipment:
Side menu > My institute shipping to send > +Create (fill in sender/receiver and name the shipment) >+Add in Item List > Add Serial Numbers of modules that are sent > Set (In Transit)

## Uploading Test Results:
- In command line -> go to production data base scripts directory (strips/code/production_database_scripts)
- Run python3 get_token.py > copy/paste in the data base scripts directory
- Run python3 upload_test_results.py —test-file <path> --component-id <serial_number(begins with 20..)> --institution=GL