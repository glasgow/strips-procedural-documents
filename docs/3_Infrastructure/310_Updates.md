# Strips Software Versions

| Revision | Date | Editor | Contents |
| -- | -- | -- | -- |
| v1 | 2024.08.16 | Kenneth Wraight | Created document |

This pages describes procedures for updating code.

Code _should_ be updated regularly.

| code | update (version id) | who |
| -- | -- | -- |
| itsdaq | ??? | ??? | ??? |
| nexys | ??? | ??? | ??? |
| coldjig | ??? | ??? | ??? |
| Zturn | ??? | ??? | ??? |
| magic spreadsheets | ??? | ??? | ??? |


## Module/hybrid testing

### itsdaq

TODO

### nexys

TODO

### coldjig

Documentation [here](https://gitlab.cern.ch/groups/ColdJigDCS/-/wikis/Updating)

1. Login to rPi

??? note "Log in through school's network"

    > ssh USERNAME@ppelogin2.ppe.gla.ac.uk

    - then to raspberryPi
    
    > ssh pi@194.36.1.65

2. Go to UK-China directory

> cd ~/ColdBox/coldjig_workspace/UK_China_Barrel

3. Use pipenv

> pipenv install


## Powerboards 

### Zturn

Use Powerbord GUI: http://194.36.1.111:5000/config 

1. Use GUI to navigate to _Configuration_ page.

2. Upload files (image.ub, BOOT.bin & boot.scr) in _Upgrade Firmware_ box.

3. Press _REBOOT_ button in _System Control_ section.

If all goes well the system should be back up ~3 minutes.

- if there are issues find Kenny/Katie.

## Interfaces

Software for sending data to PDB.

### googlesheets

Use git commands in repo.

- Repo. [here](https://gitlab.cern.ch/groups/ColdJigDCS/-/wikis/Updating)

> cd /nfs/atlas/itk-atlas/strips/code/repositories/google_sheets_with_python

> git pull origin master
