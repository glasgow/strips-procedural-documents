# Cleanroom maintenance

| Revision | Date | Editor | Contents |
| -- | -- | -- | -- |
| v1 | 2022.04.05 | Kenneth Wraight | Created document |
| v1 | 2023.12.06 | Kenneth Wraight | Update Glasgow info. |

This document describes the GLADD1 and GLADD2 cleanrooms and their regular maintenance.

## Scope

This document describes the maintenance of the cleanroom. It affects all persons using this room for any work in the scope of the project.

## Purpose

The cleanroom is a crucial part of the equipment to manufacture modules. It provides a controlled environment for safe handling of modules during manufacturing and storage.

## Definitions

__GLADD__: **GL**asgow **A**dvanced **D**etector **D**evelopment 

 - __GLADD1__: High level cleanroom

 - __GLADD2__: Low level cleanroom

## Responsibilities

Every person is responsible to maintain a clean environment. 

## PCs

Preferred user: _TestUser_ (speak to Andy, Kenny, Katie for password)

__GLADD1__:

- Linux machine: PPEPC16
    - nfs mounted
    - sudo privilege - requested

__GLADD2__:

- Windows PC: PPEPC65
    - nfs mounted
    - Labview: 2018 SP1
- Linux PC: PPEPC74
    - Alma linux
    ??? note "Anydesk"
    
        address: 1142247245

- Windows PC (old): PPEPC138
- Linux machine (old): PPEPC2
    - nfs mounted
    - sudo privilege

- __329b__: (for monitoring)
    - Linux machine: pepc21
    - nfs mounted

  
<!-- ## Procedure

### Daily maintenance

1. Needs to be done only if room in use. First person entering room is responsible and puts this on records.
2. Inspect the cleanroom for any unusual dirt. If spotted, report and trigger a weekly maintenance.
3. Check sticky mats. If excessive dirt is observed, change mats. Record this action.

### Weekly maintenance

1. Inspect all tables and equipment surfaces (gantry, wirebonder, probe station etc.). If dirt is visible, clean it using swipe pads and 2-propanole
2. Clean floor using vacuum cleaner, followed by wet cleaning using Swiffer
3. Change sticky mats (inside and outside)
4. Check gowning room. Remove unused items. Using Swiffer to clean floor (may reuse the tissue used inside cleanroom)

## Documentation

The following information needs to be recorded in the report for the Glasgow logbook:

- Date, time (start--end) and operator name
- Any special observations, e.g.~damage to parts not already recorded during visual inspection, deviations from normal procedures
- Any action taken.

---

## Regulation

### Preface

To sustain a qualified site for the module production and facilitate clean room management. We make the following rules. If there is no objection, please sign your name at the end of this files.

### In the Dressing Room

1. If you step in the dressing room, please make sure it is clean and place clothes and shoes separately in the upper and lower layers of the closet, respectively. Leaving personal belongs, including used gloves and masks, in the dressing room can be considered a violation.

2. The dressing room will be recorded by monitoring equipment for 24 hours. All violations will be publicized, and punishments will be given according to the severity and frequency of violations, as detailed in the punishment description.

3. Before entering the clean room, you must wear clothes (buttoned up), hats, gloves, masks and shoes. Do not expose skin except eyes to air. Individual clothing will be recorded by the monitoring system. Any violation will be punished as the punishment description.

### In the Clean Room

1. If you step in the clean room, make sure to wear clothes (buttoned up), hats, gloves, masks and shoes. Do not expose skin except eyes to air. Individual clothing will be recorded by the monitoring system. Any violation will be punished according to the punishment description.

2. After using any tools or instruments, please put them back to the original place. 

### Punishment

If violation happens:
 - First time: post the picture and warn through the group.\par
 - Second time: suspend access for one week.\par
 - Third time: suspend access for one month.\par
 -->
