# Power Supply Settings

| Revision | Date | Editor | Contents |
| -- | -- | -- | -- |
| v1 | 2023.12.06 | Kenneth Wraight | Created document |
| v2 | 2023.12.19 | Kenneth Wraight | Update instructions |

Describes the procedures to set-up Power Supply on Linux PC.

## GitLab

[Original gitLab](https://gitlab.cern.ch/atlas-itk-production-software/common-interfaces/google_sheets_with_python) chaos been be cloned to:

/nfs/atlas/itk-atlas/strips/code/repositories/google_sheets_with_python

??? note "Update repo."

    Once in directory:

    > git pull

    Input your (personal) gitlab details in the pop-up

## Input data

Data read from googlesheet [here](https://docs.google.com/spreadsheets/d/1nAngJBklxRyd0oDuUzrVVPphMAtbAGv2rlmIoQK1a8E/edit#gid=2141749848)

__NB__ access:

- anyone with the shared link can edit the document.
- only testUser can run the upload script (see. below)

Information on how to set up the spreadsheet:

- general set-up: [here](https://indico.cern.ch/event/1297295/contributions/5454060/attachments/2666977/4624943/2023_06_15_gsheets_database_automation.pdf)
- specifically on cell structure: [here](https://docs.google.com/document/d/16RSSelvwD3blChKodEtK9bnoC7xrT1aWIWm2ovkhCpc/edit#heading=h.laoquq47kcc1)
- ITk week presentation: [here](https://docs.google.com/presentation/d/16tU_4ETLegDO8HXRABEL9hqiKKTZIsbc_x34MZKMiLw/edit#slide=id.g21665133632_0_0)

## Glasgow running

To run the spreadsheet scripts please follow the steps below:

0. Login as testuser
    - tested on ppepc16 but _should_ work else where too
1. Navigate to cloned repo

    > cd /nfs/atlas/itk-atlas/strips/code/repositories/google_sheets_with_python

2. Start conda environment (need python 11 stuff)

    > conda activate py11

    ??? note "invalid choice error?"

        Source the activation script:

        > source /path_to_anaconda/anaconda3/bin/activate

        then run _activate_ command again

3. Run script:

    > python run_modules_LS.py --sheet_name="Glasgow_PPB2_Modules" --tab_name="PPB-LS" --column_letter=E -c="credentials/credentials.yml" -g="credentials/itk-google-sheets.json" -b "PPB2"
    
    - for more information on arguments see the git repo. [_README_](https://gitlab.cern.ch/atlas-itk-production-software/common-interfaces/google_sheets_with_python#running-the-code)

4. Quit conda (if required)

    > conda deactivate
