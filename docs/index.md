# Welcome Glasgow ITk Strips Standard Operating Procedures Documentation

This documentation is intended to help standardise operating procedures for Glasgow ITk Strips work.
 
Use the [git repo.](https://gitlab.cern.ch/glasgow/strips-procedural-documents) to make updates.

For General Information on ITk components please check [ITk-docs](https://itk.docs.cern.ch)

