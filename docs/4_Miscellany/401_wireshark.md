# Wireshark

``Wireshark is the world's foremost network protocol analyzer. It lets you see what's happening on your network at a microscopic level. It is the de facto (and often de jure) standard across many industries and educational institutions. ''

 - [Official site](https://www.wireshark.org)
 - [strips data format twiki](https://twiki.cern.ch/twiki/bin/view/Atlas/ITSDAQDataFormat)
 - [itsdaq-sw gitLab](https://gitlab.cern.ch/atlas-itk-strips-daq/itsdaq-sw/-/tree/master/www?ref_type=heads)
    - check hsioPacket.lua file for instructions
