# Inventory

| Revision | Date | Editor | Contents |
| -- | -- | -- | -- |
| v1 | 2024.11.21 | Kenneth Wraight | Created document |

Quick tool to get and search component information at Glasgow.

## Scope

Check on demand.

## Purpose

Strips to check and filter components at instiute.

## Responsibilities

People: Kenny

### Credit

Based on Paul Miyagawa's scripts on [production_database_scripts](https://gitlab.cern.ch/atlas-itk/sw/db/production_database_scripts).

## Procedure

1. Log on to PC
2. Navigate to: 

   /nfs/atlas/itk-atlas/strips/code/inventicle

3. Get components using Paul's script:

      >  source xgenCSV.sh 

   - will need to input PDB credentials

   - retrieve components of defined compenentTypes, *e.g. SENSOR HYBRID PWB PWB_CARRIER HYBRID_TEST_PANEL MODULE*

   - will get information for components at institution, *e.g. serialNumber,componentType,type,institution,currentLocation,currentStage,assembled,alternativeIdentifier,parent*

   - to change retieved componentTypes or information edit file:

      > nano xgenCSV.sh

   - __NB__ need to rerun each time you want to update inventory from PDB.

4. Filter information, examples:

   - get all downloaded information for all components:

      > python filter_csv.py --csv components.csv 

   - filter _componentType_ and _assembled_ state (_PWB_, _False_):

      > python filter_csv.py -c components.csv -f componentType=PWB assembled=False 
   
   - the data can be written to (csv) file by adding _-w_ arguments with _1_ or _true_ value, e.g

      > python filter_csv.py --c components.csv -f componentType=PWB assembled=False -w 1

5. Information should be printed to screen as table, e.g.

|     | serialNumber   | alternativeIdentifier   | componentType   | type                 | currentStage   | assembled   |
|----:|:---------------|:------------------------|:----------------|:---------------------|:---------------|:------------|
|  11 | 20USBP05009237 | None                    | PWB             | B3                   | BONDED         | False       |
|  17 | 20USBP05009235 | None                    | PWB             | B3                   | BONDED         | False       |
|  30 | 20USBP05009239 | None                    | PWB             | B3                   | BONDED         | False       |
|  36 | 20USBP05009238 | None                    | PWB             | B3                   | BONDED         | False       |
|  61 | 20USBP05009231 | None                    | PWB             | B3                   | BONDED         | False       |
|  80 | 20USBP05009233 | None                    | PWB             | B3                   | BONDED         | False       |
|  21 | 20USBP04010008 | None                    | PWB             | B3                   | HYBBURN        | False       |
|  51 | 20USBP03000062 | None                    | PWB             | B3                   | HYBBURN        | False       |
|  52 | 20USBP04010021 | None                    | PWB             | B3                   | HYBBURN        | False       |
|  58 | 20USBP03000055 | None                    | PWB             | B3                   | HYBBURN        | False       |
|  78 | 20USBP04010075 | None                    | PWB             | B3                   | HYBBURN        | False       |
|  99 | 20USBP03000012 | None                    | PWB             | B3                   | HYBBURN        | False       |
|  54 | 20USBP03000234 | None                    | PWB             | B3                   | LOADED         | False       |
| 101 | 20USBPC4040054 | 4040054                 | PWB_CARRIER     | PWB_CARRIER_20211119 | MODULE_RCP     | False       |
| 104 | 20USBPC4040055 | 4040055                 | PWB_CARRIER     | PWB_CARRIER_20211119 | MODULE_RCP     | False       |
| 107 | 20USBPC5000018 | 5000018                 | PWB_CARRIER     | PWB_CARRIER_20230315 | MODULE_RCP     | False       |

   - files will writen (if requested): _filtered_info_DATE.csv_

Some quick statistics (assmbled status and currentStage) for each componentType in the table are printed underneath the table, e.g.

```
##########
### Quick Stats
##########
PWB total: 13
 - assembled False: 13
 - currentStage BONDED: 6
 - currentStage HYBBURN: 6
 - currentStage LOADED: 1
PWB_CARRIER total: 3
 - assembled False: 3
 - currentStage MODULE_RCP: 3
```
